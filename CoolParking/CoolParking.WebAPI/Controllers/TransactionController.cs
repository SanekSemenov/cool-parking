﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/transactions")]
    [Produces("application/json")]
    public class TransactionController : Controller
    {
        private TransactionService transactionsService;

        public TransactionController()
        {
            transactionsService = new TransactionService();
        }

        [HttpGet]
        [Route("last")]
        public ActionResult<TransactionInfo> GetLast()
        {
            return
                transactionsService
                    .GetLastTransaction(); 
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                return transactionsService.GetAllTransactions();
            }
            catch (FileNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public ActionResult<Vehicle> Put(/*string id, decimal sum*/ string json)
        {
            try
            {
                return Ok(transactionsService.TopUpVehicle(json));
            }
            catch(Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }
                return NotFound(e.Message);
            }
        }
    }
}
