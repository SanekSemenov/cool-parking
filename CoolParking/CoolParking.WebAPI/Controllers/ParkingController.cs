﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI;

namespace CoolParking.WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/parking")]
    [Produces("application/json")]
    public class ParkingController : Controller
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private ParkingService ps; 

        public ParkingController()
        {
            ps = new ParkingService(_withdrawTimer, _logTimer, _logService);
        }

        [HttpGet]
        [Route("balance")]
        public decimal GetBalance()
        {
            return ps.GetBalance();
        }

        [HttpGet]
        [Route("capacity")]
        public decimal GetCapacity()
        {
            return ps.GetCapacity();
        }

        [HttpGet]
        [Route("freePlaces")]
        public decimal GetFreePlaces()
        {
            return ps.GetFreePlaces();
        }
    }
}
