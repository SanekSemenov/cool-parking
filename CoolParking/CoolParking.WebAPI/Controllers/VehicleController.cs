﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/vehicles")]
    [Produces("application/json")]
    public class VehicleController : Controller
    {
        private VehicleService vehicleService;

        public VehicleController()
        {
            vehicleService = new VehicleService();
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            return Ok(vehicleService.GetVehicles()); 
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            try
            {
                return Ok(vehicleService.GetVehicleById(id));
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public ActionResult<Vehicle> Post(string json)
        {
            try
            {
                Vehicle vehicle = vehicleService.Post(json);
                return CreatedAtAction(nameof(GetVehicleById), new { id = vehicle.Id }, vehicle);
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }
                return NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                return Ok(vehicleService.Delete(id));
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    return BadRequest(e.Message);
                }
                return NotFound(e.Message);
            }
        }
    }
}
