﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Services
{
    public class VehicleService
    {
        private static ITimerService _withdrawTimer;
        private static ITimerService _logTimer;
        private static ILogService _logService;

        private ParkingService ps = new ParkingService(_withdrawTimer, _logTimer, _logService);
        private HttpClient _client;

        public VehicleService()
        {
            _client = new HttpClient();
        }

        public IEnumerable<Vehicle> GetVehicles()
        {
            return ps.GetVehicles(); //ps.GetVehicles();
        }

        public Vehicle GetVehicleById(string id)
        {
            if (!ps.IsValidVehicleId(id))
            {
                throw new ArgumentException("ОШИБКА! ID некорректный.");
            }

            if (!ps.VehicleExistOnParking(id))
            {
                throw new DataException("ОШИБКА! Транспортного средства с таким ID нет на паркинге.");
            }

            return ps.GetVehicles().Where(x => x.Id == id).FirstOrDefault();
        }

        public Vehicle Post(string json)
        {
            Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(json);
           
            ps.AddVehicle(vehicle);
            
            return vehicle;
        }

        public string Delete(string id)
        {
            if (!ps.IsValidVehicleId(id))
            {
                throw new ArgumentException("ОШИБКА! ID некорректный.");
            }

            if (!ps.VehicleExistOnParking(id))
            {
                throw new DataException("ОШИБКА! Транспортного средства с таким ID нет на паркинге.");
            }


            ps.RemoveVehicle(id);
            return "NoContent";
        }

        public class VehicleJson
        {
            [JsonProperty("Id")]
            public string VehicleId { get; set; }
            [JsonProperty("Balance")]
            public decimal Balance { get; set; }
            [JsonProperty("VehicleType")]
            public int VehicleType { get; set; }
        }

    }
}
