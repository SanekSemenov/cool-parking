﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Services
{
    public class TransactionService
    {
        private static ITimerService _withdrawTimer;
        private static ITimerService _logTimer;
        private static ILogService _logService;
        private ParkingService ps = new ParkingService(_withdrawTimer, _logTimer, _logService);

        private HttpClient _client;

        public TransactionService()
        {
            _client = new HttpClient();
        }
        public TransactionInfo GetLastTransaction()
        {
            return ps.GetLastParkingTransactions().LastOrDefault();
        }

        public string GetAllTransactions()
        {
            return ps.ReadFromLog();
        }

        public Vehicle TopUpVehicle(string json)
        {
            TransactionInfoJson transactionInfoJson = JsonConvert.DeserializeObject<TransactionInfoJson>(json);
            
            TransactionInfo transaction = new TransactionInfo()
            {
                DatetimeFormat = DateTime.Now, Sum = transactionInfoJson.Sum, VehicleId = transactionInfoJson.VehicleId
            };
            
            if (!ps.IsValidVehicleId(transaction.VehicleId))
            {
                throw new ArgumentException("ОШИБКА! ID некорректный.");
            }

            if (!ps.VehicleExistOnParking(transaction.VehicleId))
            {
                throw new DataException("ОШИБКА! Транспортного средства с таким ID нет на паркинге.");
            }

            ps.TopUpVehicle(transaction.VehicleId, transaction.Sum);

            return ps.GetVehicles().Where(x => x.Id == transaction.VehicleId).FirstOrDefault();
        }
    }

    public class TransactionInfoJson
    {
        [JsonProperty("Id")]
        public string VehicleId { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
