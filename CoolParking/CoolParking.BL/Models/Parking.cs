﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        public decimal Balance { get; set; }
        public int Capacity { get; set; }
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public List<TransactionInfo> transactionList = new List<TransactionInfo>();
        public string logPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        private Parking()
        {
        }

        private static Parking _instance;

        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
                Settings.SettingsOn();
            }

            return _instance;
        }
    }
}