﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        private static decimal startingBalanceOfParking = 0;
        public static int Capacity = 10;
        public static double interval = 5000;
        public static int periodWriteToLog = 1;
        public static decimal penaltyRate = 2.5m;

        public static readonly Dictionary<VehicleType, decimal> Rates = new Dictionary<VehicleType, decimal>()
        {
            {VehicleType.PassengerCar, 2},
            {VehicleType.Truck, 5},
            {VehicleType.Bus, 3.5m},
            {VehicleType.Motorcycle, 1}
        };

        public static void SettingsOn()
        {
            Parking p = Parking.GetInstance();
            p.Balance = startingBalanceOfParking;
            p.Capacity = Capacity;
        }
    }
}