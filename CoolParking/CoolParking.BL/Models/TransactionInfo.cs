﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime DatetimeFormat { get; set; }
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }

        public TransactionInfo(DateTime datetimeFormat, string vehicleId, decimal sum)
        {
            DatetimeFormat = datetimeFormat;
            VehicleId = vehicleId;
            Sum = sum;
        }

    }
}