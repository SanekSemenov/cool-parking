﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;


namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public Parking p = Parking.GetInstance();

        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer =  logTimer;
            _logService = logService;
        }

        public void Dispose()
        {
        }

        public decimal GetBalance()
        {
            return p.Balance;
        }

        public int GetCapacity()
        {
            return p.Capacity;
        }

        public int GetFreePlaces()
        {
            return p.Capacity - p.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return p.Vehicles.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(!p.Vehicles.Exists(x => x.Id == vehicle.Id))
            {
                p.Vehicles.Add(vehicle);
            }
            else
            {
                throw new ArgumentException("ОШИБКА! Транспортное средства с таким ID уже находится на паркинге.");
            }
            
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (VehicleExistOnParking(vehicleId))
            {
                p.Vehicles.RemoveAll(x => x.Id == vehicleId);
            }
            else
            {
                throw new ArgumentException("ОШИБКА! Транспортного средства с таким ID нет на паркинге."); 
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum > 0)
            {
                if (VehicleExistOnParking(vehicleId))
                {
                    foreach (var el in p.Vehicles)
                    {
                        if (el.Id == vehicleId)
                        {
                            el.Balance += sum;
                        }
                    }
                }
                else
                {
                    throw new ArgumentException("ОШИБКА! Транспортного средства с таким ID нет на паркинге.");
                }
            }
            else
            {
                throw new ArgumentException("ОШИБКА! Сумма пополнения не может быть отрицательной");
            }
        }

        public string GetLastParkingTransactionsInfo()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var el in p.transactionList)
            {
                sb.Append(el.DatetimeFormat.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss.fffffffK") + ": " + el.Sum + " money withdrawn from vehicle with Id = '" + el.VehicleId + "'\n");
            }

            return sb.ToString();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return p.transactionList.ToArray();
        }

        public string ReadFromLog()
        {
            if (File.Exists(Parking.GetInstance().logPath))
            {

                LogService log = new LogService(Parking.GetInstance().logPath);
                return (log.Read());
            }
            throw new  FileNotFoundException("Файл не найден.");
        }

        public bool VehicleExistOnParking(string vehicleId)
        {
            return p.Vehicles.Exists(x => x.Id == vehicleId);
        }

        public bool IsValidVehicleId(string vehicleId)
        {
            return Regex.IsMatch(vehicleId, @"^[A-Za-z]+[A-Za-z]+-\d\d\d\d-[A-Za-z]+[A-Za-z]+$");
        }
    }
}