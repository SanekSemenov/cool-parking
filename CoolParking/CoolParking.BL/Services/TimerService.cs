﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Channels;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;


namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer t = new Timer();
        public static DateTime startDate;
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        public TimerService()
        {
            t.Interval = Settings.interval;
            t.Elapsed += OnTimedEvent;
        }

        public void Start()
        {
            t.Enabled = true;
            t.Start();
            startDate = DateTime.Now;
        }

        public void Stop()
        {
            t.Stop();
        }

        public void Dispose()
        {

        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Parking p = Parking.GetInstance();

            if (p.Vehicles.Count != 0)
            {
                foreach (var el in p.Vehicles)
                {
                    var rate = Settings.Rates.Where(x => x.Key == el.VehicleType).FirstOrDefault().Value;

                    if (el.Balance >= rate)
                    {
                        p.Balance += rate;
                        el.Balance -= rate;
                        p.transactionList.Add(new TransactionInfo(){ DatetimeFormat = e.SignalTime, VehicleId = el.Id, Sum = rate});
                    }
                    else
                    {
                        var penaltyrate = rate * Settings.penaltyRate;
                        p.Balance += penaltyrate;
                        el.Balance -= penaltyrate;
                        p.transactionList.Add(new TransactionInfo() { DatetimeFormat = e.SignalTime, VehicleId = el.Id, Sum = penaltyrate });
                    }
                }
            }

            TimeSpan difference = DateTime.Now - startDate;

            if (difference.Minutes == Settings.periodWriteToLog)
            {
                startDate = DateTime.Now;

                LogService logService = new LogService(p.logPath);
                  
                foreach (var el in p.transactionList)
                {
                    logService.Write(el.DatetimeFormat.ToString("M/dd/yyyy hh:mm:ss tt", new System.Globalization.CultureInfo("en-US")) + ": " + el.Sum + " money withdrawn from vehicle with Id = '" + el.VehicleId + "'.");
                }

                p.transactionList.Clear();
            }
        }
    }
}