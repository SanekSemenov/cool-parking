﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Dispose()
        {
            File.Delete(LogPath);
        }

        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(LogPath))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Такого файла не существует.");
            }
        }

        public string Read()
        {
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    string line;
                    StringBuilder sb = new StringBuilder();
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.Append(line + "\n");
                    }
                    return sb.ToString();
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Такого файла не существует.");
            }
        }
    }
}