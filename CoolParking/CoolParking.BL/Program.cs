﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL
{
    class Program 
    {
        public static ITimerService _withdrawTimer;
        public static ITimerService _logTimer;
        public static ILogService _logService;
        public static void Main(string[] args)
        {
            Parking p = Parking.GetInstance();
            ParkingService ps = new ParkingService(_withdrawTimer, _logTimer, _logService );
            LogService logService = new LogService(p.logPath);
            TimerService t = new TimerService();
            
            // Begin timing
            t.Start();

            int urchoice = 0;

            while (urchoice != 10)
            {
                Console.Clear();
                Console.WriteLine("\tПрограмма 'Cool Parking' приветствует тебя!");
                Console.WriteLine("\nВыберите пункт меню: ");
                Console.WriteLine("\n1. Вывести на экран текущий баланс Парковки" +
                                  "\n2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог)." +
                                  "\n3. Вывести на экран количество свободных/занятых мест на парковке." +
                                  "\n4. Вывести на экран все Транзакции Парковки за текущий период (до записи в лог)" +
                                  "\n5. Вывести историю транзакций (считав данные из файла Transactions.log)." +
                                  "\n6. Вывести на экран список Тр. средств находящихся на Паркинге" +
                                  "\n7. Поставить Тр. средство на Паркинг" +
                                  "\n8. Забрать транспортное средство с Паркинга" +
                                  "\n9. Пополнить баланс конкретного Тр. средства" +
                                  "\n10. Выход");
                Console.WriteLine("***************************************************");
                Console.WriteLine("Ваш выбор ? :");
                int.TryParse(Console.ReadLine(), out urchoice);

                switch (urchoice)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Текущий баланс парковки: " + ps.GetBalance());
                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Сумма заработанных денег за текущий период: " + ps.GetLastParkingTransactions().Sum(tr => tr.Sum));
                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Количество свободных мест: " + ps.GetFreePlaces());
                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Транзакции парковки за текущий период: ");
                        Console.WriteLine(ps.GetLastParkingTransactionsInfo());
                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("История транзакций");
                        Console.WriteLine(ps.ReadFromLog());
                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 6:
                        Console.Clear();
                        if (ps.GetFreePlaces() == Settings.Capacity)
                        {
                            Console.WriteLine("Паркинг пустой");
                            Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.WriteLine("Список транспортных средств находящихся на Паркинге: ");
                            foreach (var el in ps.GetVehicles())
                            {
                                Console.WriteLine("Id: " + el.Id + " | Тип: " + el.VehicleType + " | Баланс: " + el.Balance);
                            }
                            Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                            Console.ReadKey();
                        }
                        break;
                    case 7:
                        Console.Clear();

                        Console.WriteLine("Для того чтобы поставить Тр. средство, введите информацию о нем следующую информацию: " +
                            "\nID формата ХХ-YYYY-XX:");
                        VehicleType vehicleType = new VehicleType();
                        string vehicleId = Console.ReadLine();

                        while(!ps.IsValidVehicleId(vehicleId))
                        {
                            Console.WriteLine("Введите корректный ID: ");
                            vehicleId = Console.ReadLine();
                        }

                        Console.WriteLine("\nВыберите тип вашего тр. средства: " +
                            "\n1 - Легковое \n2 - Грузовое \n3 - Автобус \n4 - Мотоцикл");

                        Int32.TryParse(Console.ReadLine(), out int a);

                        while((a != 1) && (a != 2) && (a != 3) && (a != 4))
                        {
                            Console.WriteLine("Выбраного типа тр. средства не существует. Введите корректный: ");
                            Int32.TryParse(Console.ReadLine(), out a);
                        }
                        
                        switch (a)
                        {
                            case 1:
                                vehicleType = VehicleType.PassengerCar;
                                break;
                            case 2:
                                vehicleType = VehicleType.Truck;
                                break;
                            case 3:
                                vehicleType = VehicleType.Bus;
                                break;
                            case 4:
                                vehicleType = VehicleType.Motorcycle;
                                break;
                        }

                        try
                        {
                            ps.AddVehicle(new Vehicle(vehicleId, vehicleType, 100));
                            Console.WriteLine("УСПЕХ! Ваше транспортное средство успешно поставлено на паркинг");
                        }
                        catch(ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();

                        break;
                    case 8:
                        Console.Clear();
                        Console.WriteLine("Чтобы забрать транспортное средство, введите его Id: ");
                        
                        string vehId = Console.ReadLine();
                        
                        while (!ps.IsValidVehicleId(vehId))
                        {
                            Console.WriteLine("Введите корректный ID: ");
                            vehId = Console.ReadLine();
                        }
                        
                        try
                        {
                            ps.RemoveVehicle(vehId);
                            Console.WriteLine("УСПЕХ! Вы забрали транспортное средство с паркинга!");
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                    case 9:
                        Console.Clear();
                        Console.WriteLine("Чтобы пополнить баланс тр. средства, введите его Id: ");

                        string Id = Console.ReadLine();
                        
                        while (!ps.IsValidVehicleId(Id))
                        {
                            Console.WriteLine("Введите корректный ID: ");
                            Id = Console.ReadLine();
                        }

                        Console.WriteLine("Желаемая сумма пополнения: ");

                        Decimal.TryParse(Console.ReadLine(), out decimal sum);

                        try
                        {
                            ps.TopUpVehicle(Id, sum);
                            Console.WriteLine("УСПЕХ! Вы пополнили транспортное средство с ID " + Id + " на сумму: " + sum);
                        }
                        catch(ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        Console.WriteLine("\nНажмите любую кнопку для возвращения в меню.");
                        Console.ReadKey();
                        break;
                }
            }
            t.Stop();
            logService.Dispose();
        }
    }
}
